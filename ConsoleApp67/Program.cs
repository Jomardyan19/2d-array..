﻿using System;

namespace ConsoleApp67
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Creat and Print Array
            int[,] arr = new int[5, 5];
            Random rnd = new Random();
            int n = 5;
            int m = 5;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    arr[i, j] = rnd.Next(10, 100);
                    Console.Write(arr[i, j] + "  ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            #endregion

            #region Get Max of Line

            int maxSum = 0;
            int i1 = 0;
            for (int i = 0; i < n; i++)
            {
                int sum = 0;
                for (int j = 0; j < m; j++)
                {
                    sum += arr[i, j];
                }
                if (maxSum < sum)
                {
                    maxSum = sum;
                    i1 = i;
                }
            }
            Console.WriteLine($"maxsum= {maxSum}  line {i1}");
            Console.ReadLine();
            #endregion

        }
    }
}
